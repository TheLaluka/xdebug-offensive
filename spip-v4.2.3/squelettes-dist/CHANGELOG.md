# Changelog

## 4.2.0 - 2023-01-27

### Added

-  spip/spip#5366 Afficher le `language` des blocs de code en haut à droite

### Fixed

- #4839 Compléter les CSS responsives pour les balises `video`, `canvas`, ou `svg`
- #4851 Afficher les documents joints aux brèves dans l'espace public
- spip/spip#5351 Distinguer les styles du .spip_code inline et block
- #3834 Réparer mieux la fonctionnalité d'embed de document unique (non image) sur les articles vides
- #4847 Permettre d'insérer deux formulaires de recherche dans la même page
- #4845 Éviter que les paginations débordent sur petit écran

### Removed

- spip/spip#5343 suppression du filtre |lignes_longues maintenant géré en css
- spip/spip#5402 Suppression des 3 formulaires inscription mot_de_passe et oubli (déplacés dans le core)
