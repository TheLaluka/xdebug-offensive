# Changelog

## 3.1.0 - 2023-01-27

### Added

- spip/safehtml#4786 Ajout du Sanitizer SVG auparavant dans le plugin medias
- Fichier `README.md`

### Changed

- spip/spip#5271 Utilise HTMLPurifier à la place de SafeHTML
- Conversion des tests unitaires en PHPUnit
- Compatible SPIP 4.2.0-dev
