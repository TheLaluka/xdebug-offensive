# Changelog

## 3.1.3 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Changed

- Mise à jour des chaînes de langues depuis trad.spip.net

## 3.1.2 - 2023-02-27

### Changed

- Mise à jour des chaînes de langues depuis trad.spip.net


## 3.1.1 - 2023-02-23

### Added

- #4827 Ajout d’un événement `markItUpEditor.loaded` lorsque les barres d’édition sont chargées.


## 3.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Changed

- #4825 Utiliser backtick et triple backtick pour les blocs de code
- Compatible SPIP 4.2.0-dev
