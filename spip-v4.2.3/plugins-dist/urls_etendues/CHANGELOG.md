# Changelog

## 4.1.1 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Fixed

- #4826 Permettre de sélectionner le texte de l'url des objets

## 4.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Chaged

- Compatible SPIP 4.2.0-dev

