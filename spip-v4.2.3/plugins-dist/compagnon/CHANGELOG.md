# Changelog

## 3.1.1 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Fixed

- Un message d’accueil affiche correctement le nom du site s’il contient une balise multi.


## 3.1.0 - 2023-01-27

### Added

- Fichier README.md

### Changed

- Compatible SPIP 4.2-dev
