# Changelog

## 4.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Changed

- #4831 #4828 Utiliser l'API parents/enfants pour listes les objets enfants de chaque rubrique, mais il faut quand même que les objets concernés proposent un squelette `prive/squelettes/inclure/plan-{table}.html` pour qu'ils apparaissent dans le plan
- #4830 Feuille de style plus flexible et icones SVG
- Compatible SPIP 4.2.0-dev
