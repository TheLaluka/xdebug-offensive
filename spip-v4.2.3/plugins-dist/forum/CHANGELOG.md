# Changelog

## 3.1.1 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.


## 3.1.0 - 2023-01-27

### Added

- Fichier `CHANGELOG.md`

### Changed

- Compatible SPIP 4.2.0-dev

### Fixed

- #4765 Ajout des icones SVG manquantes sur la page de gestion des forums internes
- spip/spip#5274 Homogénéiser les labels des listes
- #4762 Limiter le nombre d'items à 100 dans les RSS produits

### Removed

- #5343 suppression du filtre |lignes_longues maintenant géré en css
