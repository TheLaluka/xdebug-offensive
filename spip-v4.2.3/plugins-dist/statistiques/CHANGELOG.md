# Changelog

## 3.1.2 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Changed

- Mise à jour des chaînes de langues depuis trad.spip.net

### Fixed

- #4868 fix le fonctionnement avec le plugin statistiques_objets
- #4864 Afficher les articles dont le titre est vide


## 3.1.1 - 2023-02-23

### Fixed

- #4865 Avoir un espace blanc entre la chaine visites et son referer
- #4860 correction de warnings dans genie/visites

## 3.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Changed

- Nécessite SPIP 4.2.0-dev minimum
