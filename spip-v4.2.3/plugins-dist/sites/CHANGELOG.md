# Changelog

## 4.1.2 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.


## 4.1.1 - 2023-02-27

### Changed

- Mise à jour des chaînes de langues depuis trad.spip.net


## 4.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Changed

- Compatible SPIP 4.2.0-dev

### Fixed

- spip/spip#5274 Homogénéiser les labels des listes
- spip/spip#5156 Ne pas envoyer tout spip_meta dans la config des formulaires
- #4846 Filtrer les sites par présence d'une syndication ou pas

### Removed

- spip/spip#5343 suppression du filtre `|lignes_longues` maintenant géré en css
