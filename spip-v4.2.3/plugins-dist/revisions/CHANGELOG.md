# Changelog

## 3.1.2 - 2023-06-07

### Fixed

- #4849 Bouton retour sur les articles avec un titre vide
- #4845 Bouton retour sur les documents

## 3.1.1 - 2023-05-25

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Fixed

- #4850 Bien charger les autorisations `voirrevisions` et `revisions_menu`

## 3.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Changed

- Compatible SPIP 4.2.0-dev

### Fixed

- #4843 Correction du RSS du suivi des révisions
- spip/spip#5274 Homogénéiser les labels des listes
