# Changelog

## 3.1.3 - 2023-05-16

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Fixed

- #4888 Couleur de la barre de progression sous webkit
- #4884 Aligner l'affichage de la liste des plugins de `exec=admin_plugin` sur celle de `exec=charger_plugin`
- #4879 La migration de base depuis 0.4.0 générait des erreurs par absence du champ 'procure'

## 3.1.2 - 2023-02-28

### Fixed

- #4877 Corriger les actions en cascade lors de l’installation ou désinstallation des plugins


## 3.1.1 - 2023-02-23

### Changed

- #4876 Utiliser `Minipage\Admin` sur l’écran de progression d’installation des plugins


## 3.1.0 - 2023-01-27

### Added

- Fichier `README.md`

### Changed

- #4870 Accepter des bornes du type, `x`, `x.y`, et `x.y.z` dans un intervalle de compatibilité
- spip/spip#5156 Ne pas envoyer tout spip_meta dans la config des formulaires
- Compatible SPIP 4.2.0-dev

### Fixed

- spip/spip#5274 Homogénéiser les labels des listes
