# Changelog

## 3.1.2 - 2023-06-07

### Added

- #4857 spip-contrib-extensions/oembed#26 Pipelines `pre_echappe_html_propre_args` et `post_echappe_html_propre_args`

### Fixed

- #4855 0 et '0' sont des textes à traiter aussi

## 3.1.1 - 2023-02-23

### Fixed

- #4853 Deprecated en PHP 8.1+ (+typage de fonctions)

## 3.1.0 - 2023-01-27

### Added

- #4843 Support des backticks pour insérer du code dans le contenu éditorial
- Fichier `README.md`

### Changed

- spip/spip#5271 Refactoring de la mise en sécurité des textes
- Conversion des tests unitaires en PHPUnit
- Compatible SPIP 4.2.0-dev

### Removed

- #4836 `create_replace` n'est plus un type de règle supporté
