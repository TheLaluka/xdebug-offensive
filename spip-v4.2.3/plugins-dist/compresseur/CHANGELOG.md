# Changelog

## 2.1.2 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.


## 2.1.1 - 2023-02-23

### Fixed

- #4856 Warning lors de la concaténation des `@import`

## 2.1.0 - 2023-01-27

### Added

- Fichier README.md

### Changed

- Mise à jour CSSTidy v2.0.3
- Refactoring des tests unitaires avec PHPUnit
- Compatible SPIP 4.2-dev

### Removed

- Gestion de flag_ob (supprimé de SPIP 4.2)
