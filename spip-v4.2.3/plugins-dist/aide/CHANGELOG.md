# Changelog

## 3.1.0 - 2023-01-27

## Added

- Fichier `README.md`

## Changed

- Compatible SPIP 4.2.0-dev

## Fixed

- #4619 correction d'une typo dans aide/fr/raccourcis/resume.spip