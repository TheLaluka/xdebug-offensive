# Changelog

## 3.1.2 - 2023-06-07

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Added

- #4876 Mode d'affichage "sidebar" via l'attribut `data-box-sidebar`
- #4876 Ajouter des classes arbitraires au conteneur général via l'attribut `data-box-class`

### Fixed

- #4873 Support des images webp

## 3.1.1 - 2023-02-27

### Changed

- Mise à jour des chaînes de langues depuis trad.spip.net


## 3.1.0 - 2023-01-27

### Added

- Fichier `README.md`

## Changed

- Compatible SPIP 4.2.0-dev

## Fixed

- Compatibilité PHP 8.2-dev
