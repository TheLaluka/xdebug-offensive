/*
   +----------------------------------------------------------------------+
   | Xdebug                                                               |
   +----------------------------------------------------------------------+
   | Copyright (c) 2002-2022 Derick Rethans                               |
   +----------------------------------------------------------------------+
   | This source file is subject to version 1.01 of the Xdebug license,   |
   | that is bundled with this package in the file LICENSE, and is        |
   | available at through the world-wide-web at                           |
   | https://xdebug.org/license.php                                       |
   | If you did not receive a copy of the Xdebug license and are unable   |
   | to obtain it through the world-wide-web, please send a note to       |
   | derick@xdebug.org so we can mail you a copy immediately.             |
   +----------------------------------------------------------------------+
 */
#include "lib/php-header.h"
#include "ext/standard/php_string.h"

#include "php_xdebug.h"
#include "tracing_private.h"
#include "trace_textual.h"

#include "lib/lib_private.h"
#include "lib/var_export_line.h"

extern ZEND_DECLARE_MODULE_GLOBALS(xdebug);

void *xdebug_trace_textual_init(char *fname, zend_string *script_filename, long options)
{
	xdebug_trace_textual_context *tmp_textual_context;

	tmp_textual_context = xdmalloc(sizeof(xdebug_trace_textual_context));
	tmp_textual_context->trace_file = xdebug_trace_open_file(fname, script_filename, options);

	if (!tmp_textual_context->trace_file)
	{
		xdfree(tmp_textual_context);
		return NULL;
	}

	return tmp_textual_context;
}

void xdebug_trace_textual_deinit(void *ctxt)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;

	xdebug_file_close(context->trace_file);
	xdebug_file_dtor(context->trace_file);
	context->trace_file = NULL;

	xdfree(context);
}

void xdebug_trace_textual_write_header(void *ctxt)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;
	char *str_time;

	str_time = xdebug_nanotime_to_chars(xdebug_get_nanotime(), 6);
	xdebug_file_printf(context->trace_file, "TRACE START [%s]\n", str_time);
	xdfree(str_time);

	xdebug_file_flush(context->trace_file);
}

void xdebug_trace_textual_write_footer(void *ctxt)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;
	char *str_time;
	uint64_t nanotime;
	char *tmp;

	nanotime = xdebug_get_nanotime();
	tmp = xdebug_sprintf("%10.4F ", XDEBUG_SECONDS_SINCE_START(nanotime));
	xdebug_file_printf(context->trace_file, "%s", tmp);
	xdfree(tmp);
	xdebug_file_printf(context->trace_file, "%10zu", zend_memory_usage(0));
	xdebug_file_printf(context->trace_file, "\n");

	str_time = xdebug_nanotime_to_chars(nanotime, 6);
	xdebug_file_printf(context->trace_file, "TRACE END   [%s]\n\n", str_time);
	xdfree(str_time);

	xdebug_file_flush(context->trace_file);
}

char *xdebug_trace_textual_get_filename(void *ctxt)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;

	return context->trace_file->name;
}

static void add_single_value(xdebug_str *str, zval *zv)
{
	xdebug_str *tmp_value = NULL;

	tmp_value = xdebug_get_zval_value_line(zv, 0, NULL);

	if (tmp_value)
	{
		xdebug_str_add_str(str, tmp_value);
		xdebug_str_free(tmp_value);
	}
	else
	{
		xdebug_str_add_literal(str, "???");
	}
}

static void add_arguments(xdebug_str *line_entry, function_stack_entry *fse)
{
	unsigned int j = 0; /* Counter */
	int c = 0;			/* Comma flag */
	int variadic_opened = 0;
	int variadic_count = 0;
	int sent_variables = fse->varc;

	if (sent_variables > 0 && fse->var[sent_variables - 1].is_variadic && Z_ISUNDEF(fse->var[sent_variables - 1].data))
	{
		sent_variables--;
	}

	for (j = 0; j < sent_variables; j++)
	{
		if (c)
		{
			xdebug_str_add_literal(line_entry, ", ");
		}
		else
		{
			c = 1;
		}

		if (fse->var[j].is_variadic)
		{
			xdebug_str_add_literal(line_entry, "...");
			variadic_opened = 1;
			c = 0;
		}

		if (fse->var[j].name)
		{
			xdebug_str_addc(line_entry, '$');
			xdebug_str_add_zstr(line_entry, fse->var[j].name);
			if (variadic_opened && !fse->var[j].is_variadic)
			{
				xdebug_str_add_literal(line_entry, " => ");
			}
			else
			{
				xdebug_str_add_literal(line_entry, " = ");
			}
		}

		if (fse->var[j].is_variadic)
		{
			xdebug_str_add_literal(line_entry, "variadic(");
			if (Z_ISUNDEF(fse->var[j].data))
			{
				continue;
			}
			c = 1;
		}

		if (variadic_opened && (!fse->var[j].name || fse->var[j].is_variadic))
		{
			xdebug_str_add_fmt(line_entry, "%d => ", variadic_count++);
		}

		if (!Z_ISUNDEF(fse->var[j].data))
		{
			add_single_value(line_entry, &fse->var[j].data);
		}
		else
		{
			xdebug_str_add_literal(line_entry, "???");
		}
	}

	if (variadic_opened)
	{
		xdebug_str_addc(line_entry, ')');
	}
}
// https://gist.github.com/mccabe615/b0907514d34b2de088c4996933ea1720
int checkFunctionName(const char *function_name)
{
	const char *function_names[] = {
									// "according",
									// "apache_child_terminate",
									// "apart",
									// "array_diff_uassoc",
									// "array_diff_ukey",
									// "array_filter",
									// "array_intersect_uassoc",
									// "array_intersect_ukey",
									// "array_map",
									// "array_reduce",
									// "array_udiff",
									// "array_udiff_assoc",
									// "array_udiff_uassoc",
									// "array_uintersect",
									// "array_uintersect_assoc",
									// "array_uintersect_uassoc",
									// "array_walk",
									// "array_walk_recursive",
									// "assert",
									// "assert_options",
									// "base64_decode",
									"bzopen",
									// "call_user_func",
									// "call_user_func_array",
									// "chgrp",
									// "chmod",
									// "chown",
									"copy",
									// "create_function",
									// "disk_free_space",
									// "disk_total_space",
									// "diskfreespace",
									"eval",
									"exec",
									"exif_imagetype",
									// "exif_read_data",
									// "exif_thumbnail",
									// "extract",
									// "fclose",
									"fgets",
									"file",
									"file_exists",
									"file_get_contents",
									"file_put_contents",
									"fileatime",
									"filectime",
									"filegroup",
									"fileinode",
									"filemtime",
									"fileowner",
									"fileperms",
									"filesize",
									"filesystem",
									"filetype",
									"fopen",
									// "fread",
									"fsockopen",
									// "ftp_get",
									// "ftp_nb_get",
									// "ftp_nb_put",
									// "ftp_put",
									// "function",
									// "fwrite",
									// "get_cfg_var",
									// "get_current_user",
									// "get_meta_tags",
									// "getcwd",
									// "getenv",
									"getimagesize",
									// "getlastmo",
									// "getmygid",
									// "getmyinode",
									// "getmypid",
									// "getmyuid",
									// "glob",
									"gzfile",
									"gzopen",
									"hash_file",
									"hash_hmac_file",
									"hash_update_file",
									// "header",
									"highlight_file",
									"image2wbmp",
									"imagecreatefromgif",
									"imagecreatefromjpeg",
									"imagecreatefrompng",
									"imagecreatefromwbmp",
									"imagecreatefromxbm",
									"imagecreatefromxpm",
									"imagegd",
									"imagegd2",
									"imagegif",
									"imagejpeg",
									"imagepng",
									"imagewbmp",
									"imagexbm",
									// "include",
									// "include_once",
									// "information",
									// "ini_set",
									// "invoke",
									// "iptcembed",
									"is_dir",
									// "is_executable",
									"is_file",
									// "is_link",
									// "is_readable",
									"is_uploaded_file",
									// "is_writable",
									// "is_writeable",
									// "iterator_apply",
									// "lcg_value",
									// "lchgrp",
									// "lchown",
									// "libxml_disable_entity_loader",
									// "link",
									// "linkinfo",
									// "list",
									// "lstat",
									// "mail",
									"md5_file",
									// "mhash_",
									// "mkdir",
									// "most",
									"move_uploaded_file",
									// "mt_srand",
									// "mysql_connect",
									// "mysqli",
									// "ob_start",
									"openssl_",
									// "other",
									"parse_ini_file",
									// "parse_str",
									"parsekit_compile_file",
									"pass",
									"passthru",
									"password",
									// "pathinfo",
									"pcntl_exec",
									"pfsockopen",
									// "php",
									// "php_check_syntax",
									// "php_strip_whitespace",
									// "PHPExecute",
									// "phpinfo",
									"popen",
									// "posix_getlogin",
									// "posix_kill",
									// "posix_mkfifo",
									// "posix_setpgid",
									// "posix_setsid",
									// "posix_setuid",
									// "posix_ttyname",
									// "preg_replace",
									// "preg_replace_callback",
									// "proc_close",
									// "proc_get_status",
									// "proc_nice",
									"proc_open",
									// "proc_terminate",
									// "putenv",
									// "rand",
									// "random_",
									// "read_exif_data",
									"readfile",
									"readgzfile",
									// "readlink",
									// "realpath",
									// "register_shutdown_function",
									// "register_tick_function",
									// "rename",
									// "require",
									// "require_once",
									// "rmdir",
									// "runkit_import",
									// "session_set_save_handler",
									// "set_error_handler",
									// "set_exception_handler",
									// "set_include_path",
									"sha1_file",
									"shell_exec",
									"show_source",
									// "spl_autoload_register",
									// "sqlite_create_aggregate",
									// "sqlite_create_function",
									// "stat",
									// "symlink",
									"system",
									// "tempnam",
									// "these",
									"tmpfile",
									// "touch",
									// "uasort",
									// "uksort",
									// "unlink",
									// "unserialize",
									// "username",
									// "usort",
									// "virtual"
	};

	size_t num_functions = sizeof(function_names) / sizeof(function_names[0]);

	for (size_t i = 0; i < num_functions; i++)
	{
		if (strcmp(function_name, function_names[i]) == 0)
		{
			// Match found
			return 0;
		}
	}

	// No match found
	return 1;
}

void xdebug_trace_textual_function_entry(void *ctxt, function_stack_entry *fse, int function_nr)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;
	unsigned int j = 0; /* Counter */
	char *tmp_name;
	xdebug_str str = XDEBUG_STR_INITIALIZER;

	int result = 0;
	tmp_name = xdebug_show_fname(fse->function, XDEBUG_SHOW_FNAME_DEFAULT);
	// printf("LALULOG: %s\n", tmp_name);
	result = checkFunctionName(tmp_name);
	if (result == 0)
	{
		printf("Filter(OK):  Logging function '%s'\n", tmp_name);
	}
	else
	{
		printf("Filter(KO): Skipping function '%s'\n", tmp_name);
		return;
	}

	xdebug_str_add_fmt(&str, "%10.4F ", XDEBUG_SECONDS_SINCE_START(fse->nanotime));
	xdebug_str_add_fmt(&str, "%10lu ", fse->memory);
	for (j = 0; j < fse->level; j++)
	{
		xdebug_str_add_literal(&str, "  ");
	}
	xdebug_str_add_fmt(&str, "-> %s(", tmp_name);

	xdfree(tmp_name);

	add_arguments(&str, fse);

	if (fse->include_filename)
	{
		if (fse->function.type == XFUNC_EVAL)
		{
			zend_string *escaped;

			escaped = php_addcslashes(fse->include_filename, (char *)"'\\\0..\37", 6);

			xdebug_str_addc(&str, '\'');
			xdebug_str_add_zstr(&str, escaped);
			xdebug_str_addc(&str, '\'');
			zend_string_release(escaped);
		}
		else
		{
			xdebug_str_add_zstr(&str, fse->include_filename);
		}
	}

	xdebug_str_add_fmt(&str, ") %s:%d\n", ZSTR_VAL(fse->filename), fse->lineno);

	xdebug_file_printf(context->trace_file, "%s", str.d);
	xdebug_file_flush(context->trace_file);

	xdfree(str.d);
}

/* Used for normal return values, and generator return values */
static void xdebug_return_trace_stack_common(xdebug_str *str, function_stack_entry *fse)
{
	unsigned int j = 0; /* Counter */

	xdebug_str_add_fmt(str, "%10.4F ", XDEBUG_SECONDS_SINCE_START(xdebug_get_nanotime()));
	xdebug_str_add_fmt(str, "%10lu ", zend_memory_usage(0));

	for (j = 0; j < fse->level; j++)
	{
		xdebug_str_add_literal(str, "  ");
	}
	xdebug_str_add_literal(str, " >=> ");
}

void xdebug_trace_textual_function_return_value(void *ctxt, function_stack_entry *fse, int function_nr, zval *return_value)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;
	xdebug_str str = XDEBUG_STR_INITIALIZER;
	xdebug_str *tmp_value;

	xdebug_return_trace_stack_common(&str, fse);

	tmp_value = xdebug_get_zval_value_line(return_value, 0, NULL);
	if (tmp_value)
	{
		xdebug_str_add_str(&str, tmp_value);
		xdebug_str_free(tmp_value);
	}
	xdebug_str_addc(&str, '\n');

	xdebug_file_printf(context->trace_file, "%s", str.d);
	xdebug_file_flush(context->trace_file);

	xdebug_str_destroy(&str);
}

void xdebug_trace_textual_generator_return_value(void *ctxt, function_stack_entry *fse, int function_nr, zend_generator *generator)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;
	xdebug_str str = XDEBUG_STR_INITIALIZER;
	xdebug_str *tmp_value = NULL;

	if (!(generator->flags & ZEND_GENERATOR_CURRENTLY_RUNNING))
	{
		return;
	}

	if (generator->execute_data == NULL)
	{
		return;
	}

	/* Generator key */
	tmp_value = xdebug_get_zval_value_line(&generator->key, 0, NULL);
	if (!tmp_value)
	{
		return;
	}

	xdebug_return_trace_stack_common(&str, fse);

	xdebug_str_addc(&str, '(');
	xdebug_str_add_str(&str, tmp_value);
	xdebug_str_add_literal(&str, " => ");

	tmp_value = xdebug_get_zval_value_line(&generator->value, 0, NULL);
	if (tmp_value)
	{
		xdebug_str_add_str(&str, tmp_value);
		xdebug_str_free(tmp_value);
	}

	xdebug_str_add_literal(&str, ")\n");

	xdebug_file_printf(context->trace_file, "%s", str.d);
	xdebug_file_flush(context->trace_file);

	xdebug_str_destroy(&str);
}

void xdebug_trace_textual_assignment(void *ctxt, function_stack_entry *fse, char *full_varname, zval *retval, char *right_full_varname, const char *op, char *filename, int lineno)
{
	xdebug_trace_textual_context *context = (xdebug_trace_textual_context *)ctxt;
	unsigned int j = 0;
	xdebug_str str = XDEBUG_STR_INITIALIZER;
	xdebug_str *tmp_value;

	xdebug_str_add_literal(&str, "                    ");
	for (j = 0; j <= fse->level; j++)
	{
		xdebug_str_add_literal(&str, "  ");
	}
	xdebug_str_add_literal(&str, "   => ");

	xdebug_str_add(&str, full_varname, 0);

	if (op[0] != '\0')
	{ /* pre/post inc/dec ops are special */
		xdebug_str_addc(&str, ' ');
		xdebug_str_add(&str, op, 0);
		xdebug_str_addc(&str, ' ');

		if (right_full_varname)
		{
			xdebug_str_add(&str, right_full_varname, 0);
		}
		else
		{
			tmp_value = xdebug_get_zval_value_line(retval, 0, NULL);

			if (tmp_value)
			{
				xdebug_str_add_str(&str, tmp_value);
				xdebug_str_free(tmp_value);
			}
			else
			{
				xdebug_str_add_literal(&str, "NULL");
			}
		}
	}
	xdebug_str_add_fmt(&str, " %s:%d\n", filename, lineno);

	xdebug_file_printf(context->trace_file, "%s", str.d);
	xdebug_file_flush(context->trace_file);

	xdfree(str.d);
}

xdebug_trace_handler_t xdebug_trace_handler_textual =
	{
		xdebug_trace_textual_init,
		xdebug_trace_textual_deinit,
		xdebug_trace_textual_write_header,
		xdebug_trace_textual_write_footer,
		xdebug_trace_textual_get_filename,
		xdebug_trace_textual_function_entry,
		NULL /*xdebug_trace_textual_function_exit */,
		xdebug_trace_textual_function_return_value,
		xdebug_trace_textual_generator_return_value,
		xdebug_trace_textual_assignment};
